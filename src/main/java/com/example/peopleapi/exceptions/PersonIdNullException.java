package com.example.peopleapi.exceptions;

public class PersonIdNullException extends RuntimeException {

    public PersonIdNullException() {
    }

    public PersonIdNullException(String message) {
        super(message);
    }

    public PersonIdNullException(String message, Throwable cause) {
        super(message, cause);
    }

    public PersonIdNullException(Throwable cause) {
        super(cause);
    }

    public PersonIdNullException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
