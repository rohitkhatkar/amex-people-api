package com.example.peopleapi.exceptions;

public class PeopleNotFoundException extends RuntimeException {
    private static final String messageTemplate = "People with id %s not found.";

    public PeopleNotFoundException() {
    }

    public PeopleNotFoundException(String message) {
        super(String.format(messageTemplate, message));
    }

    public PeopleNotFoundException(String message, Throwable cause) {
        super(String.format(messageTemplate, message), cause);
    }

    public PeopleNotFoundException(Throwable cause) {
        super(cause);
    }

    public PeopleNotFoundException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(String.format(messageTemplate, message), cause, enableSuppression, writableStackTrace);
    }
}
