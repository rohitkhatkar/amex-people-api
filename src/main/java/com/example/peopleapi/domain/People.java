package com.example.peopleapi.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import java.time.ZonedDateTime;

@Entity
public class People {
    @Id
    private String id;
    @NotNull
    private String name;
    @NotNull
    private Integer age;
    @Past
    @NotNull
    private ZonedDateTime date;
    @Email
    @Column(unique = true)
    @NotNull
    private String email;

    public People() {
    }

    public People(@NotNull String name, @NotNull Integer age, @Past @NotNull ZonedDateTime date,
                  @Email @NotNull String email) {
        this.name = name;
        this.age = age;
        this.date = date;
        this.email = email;
    }

    public People(@NotNull String id, @NotNull String name, @NotNull Integer age, @Past ZonedDateTime date,
                  @Email String email) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.date = date;
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public ZonedDateTime getDate() {
        return date;
    }

    public void setDate(ZonedDateTime date) {
        this.date = date;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "People{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", age=" + age +
                ", date=" + date +
                ", email='" + email + '\'' +
                '}';
    }
}
