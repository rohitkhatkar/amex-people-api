package com.example.peopleapi.controller.rest;

import com.example.peopleapi.domain.People;
import com.example.peopleapi.exceptions.ErrorMessage;
import com.example.peopleapi.exceptions.PeopleNotFoundException;
import com.example.peopleapi.exceptions.PersonIdNullException;
import com.example.peopleapi.services.PeopleService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Objects;

@RestController
@RequestMapping(value = "/people", produces = MediaType.APPLICATION_JSON_VALUE)
public class PeopleController {

    private final Logger logger = LoggerFactory.getLogger(PeopleController.class);

    private PeopleService peopleService;

    public PeopleController(PeopleService peopleService) {
        this.peopleService = peopleService;
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public List<People> peopleList() {
        return peopleService.getListOfAllPeople();
    }

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public People onePeopleById(@PathVariable(value = "id", required = true) String id) {
        return peopleService.getPeopleById(id);
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public People savePeople(@RequestBody @Validated People people) {
        return peopleService.savePeople(people);
    }

    @PutMapping(consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public People replacePeople(@RequestBody @Validated People people) {
        if (Objects.isNull(people.getId())) {
            throw new PersonIdNullException("People resource id should not be null.");
        }
        return peopleService.replacePeople(people);
    }

    @DeleteMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deletePeople(@PathVariable(value = "id", required = true) String id) {
        peopleService.deletePeopleById(id);
    }

    @ExceptionHandler({DataIntegrityViolationException.class, EmptyResultDataAccessException.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorMessage handleException(Exception e) {
        return new ErrorMessage(e.getMessage());
    }
    @ExceptionHandler({PeopleNotFoundException.class})
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ErrorMessage handlePeopleNotFoundException(Exception e) {
        return new ErrorMessage(e.getMessage());
    }

    @ExceptionHandler({PersonIdNullException.class})
    @ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
    public ErrorMessage handleNullIdException(Exception e) {
        return new ErrorMessage(e.getMessage());
    }
}
