package com.example.peopleapi.respository;

import com.example.peopleapi.domain.People;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PeopleRepository extends CrudRepository<People, String> {

}
