package com.example.peopleapi.services;

import com.example.peopleapi.domain.People;
import com.example.peopleapi.exceptions.PeopleNotFoundException;
import com.example.peopleapi.respository.PeopleRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
public class PeopleService {
    private final Logger logger = LoggerFactory.getLogger(PeopleService.class);

    private PeopleRepository peopleRepository;

    public PeopleService(PeopleRepository peopleRepository) {
        this.peopleRepository = peopleRepository;
    }

    public List<People> getListOfAllPeople() {
        List<People> peopleList = new ArrayList<>();
        peopleRepository.findAll().forEach(peopleList::add);
        return peopleList;
    }

    public People getPeopleById(String id) {
        return peopleRepository.findById(id).orElseThrow(() -> new PeopleNotFoundException(id));
    }

    public void deletePeopleById(String id) {
        try {
            peopleRepository.deleteById(id);
        }catch (EmptyResultDataAccessException e){
                throw new PeopleNotFoundException(id);
        }
    }

    public People savePeople(People people) {
        people.setId(UUID.randomUUID().toString());
        return peopleRepository.save(people);
    }

    public People replacePeople(People people) {
        return peopleRepository.save(people);
    }
}
