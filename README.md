Index page should redirect you to swagger web ui.

Sample Records:

[
    {
        "id": "fc858537-f2b0-49d6-b503-713a4e4842d8",
        "name": "Some Name",
        "age": 29,
        "date": "2018-08-06T01:29:58.16Z",
        "email": "tony@example.org"
    },
    {
        "id": "e5c16c9a-601d-42bc-b00c-39360087a884",
        "name": "Some Name2",
        "age": 26,
        "date": "2018-08-06T01:29:58.16Z",
        "email": "tony2@example.org"
    }
]